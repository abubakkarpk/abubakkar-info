import React, { useEffect, useRef, useState } from 'react'

const ContactForm = () => {
  let focusRef = useRef()
  let [data, setData] = useState({
    name: '',
    email: '',
    telephone: '',
    subject: '',
    message: '',
  })

  useEffect(() => {
    focusRef.current.focus()
  }, [])

  const handleChange = ({target: {name, value}}) => {
    setData({...data, [name]: value})
  }

  const resetForm = () => {
    setData({name: '', email: '', telephone: '', subject: '', message: ''})
  }

  const handleSubmit = event => {
    event.preventDefault()
    console.log(data)
    resetForm()
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className='form-group'>
        <label className='form-label'>Name</label>
        <input 
          name='name' 
          value={data.name}
          ref={focusRef}
          onChange={handleChange} 
          placeholder='Full Name' 
          className='form-control'
        />
      </div>
      <div className='form-group'>
        <label className='form-label'>Email</label>
        <input 
          type="email" 
          name='email' 
          value={data.email} 
          onChange={handleChange} 
          placeholder='Email Address' 
          className='form-control'
        />
      </div>
      <div className='form-group'>
        <label className='form-label'>Telephone No (Optional)</label>
        <input 
          name='telephone' 
          value={data.telephone} 
          onChange={handleChange} 
          placeholder='03XX XXXXXXX' 
          className='form-control'
        />
      </div>
      <div className='form-group'>
        <label className='form-label'>Subject</label>
        <input 
          name='subject' 
          value={data.subject} 
          onChange={handleChange} 
          placeholder='How can I help you?' 
          className='form-control'
        />
      </div>
      <div className='form-group'>
        <label className='form-label'>Message</label>
        <textarea 
          rows="5" 
          name='message'
          value={data.message}
          onChange={handleChange}
          placeholder='Your message...' 
          className='form-control'
        ></textarea>
      </div>
      <div className='group mt-5'>
        <button type="submit" className='btn btn-blue'>Send</button>
      </div>
    </form>
  )
}

export default ContactForm
