import React from 'react'
import PropTypes from 'prop-types'
import BulletList from 'components/BulletList'

const JobSummary = ({ job }) => {
    return (
        <div className='my-5'>
            <div className='flex justify-between mb-2'>
                <div>
                    <h4 className='font-bold text-xl'>{job.company.name}</h4>
                    <p className='font-medium'>{job.company.type}</p>
                </div>
                <div className='font-bold'>
                    <p>{job.duration.from} - {job.duration.to}</p>
                </div>
            </div>
            
            <div className='space-y-2'>
                <div>
                    {job.summary}
                </div>
                {
                    job.responsibilities.length 
                        ? <BulletList name='Key Responsibilities' items={job.responsibilities} />
                        : null 

                }
                {
                    job.projects.length 
                        ? <BulletList name='Projects' items={job.projects} />
                        : null 

                }
                {
                    job.achievements.length 
                        ? <BulletList name='Achievements / Rewards' items={job.achievements} />
                        : null 

                }
            </div>
        </div>
    )
}

JobSummary.propTypes = {
    job: PropTypes.shape({
        duration: PropTypes.shape({
            from: PropTypes.string,
            to: PropTypes.string,
        }),
        company: PropTypes.shape({
            name: PropTypes.string,
            type: PropTypes.string,
        }),
        summary: PropTypes.string,
        responsibilities: PropTypes.arrayOf(PropTypes.string),
        projects: PropTypes.arrayOf(PropTypes.string),
        achievements: PropTypes.arrayOf(PropTypes.string),
    })
}

export default JobSummary