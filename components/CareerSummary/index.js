import React from 'react'
import PropTypes from 'prop-types'
import SectionHeader from 'components/SectionHeader'
import JobSummary from './JobSummary'

const CareerSummary = ({ jobs }) => {
    return (
        <section id='career'>
            <SectionHeader text="Career Summary" />
            {
                jobs.map(job => <JobSummary job={job} key={job.id} />)
            }
        </section>
    )
}

CareerSummary.defaultProps = {
    jobs: []
}

CareerSummary.propTypes = {
    jobs: PropTypes.array,
}

export default CareerSummary