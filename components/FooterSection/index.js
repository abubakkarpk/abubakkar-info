import React from 'react'

const FooterSection = () => {
    return (
        <footer className='py-5 text-center'>
            2020 &copy; Abu Bakkar Siddique
        </footer>
    )
}

export default FooterSection