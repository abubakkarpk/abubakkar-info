import React from 'react'
import PropTypes from 'prop-types'

import SectionHeader from 'components/SectionHeader'
import BulletList from 'components/BulletList'

const Qualification = ({ list }) => {
    return (
        <section id='qualification'>
            <SectionHeader text='Education / Qualification'/>
            <BulletList items={list} />
        </section>
    )
}

Qualification.propTypes = {
    list: PropTypes.arrayOf(PropTypes.string)    
}

export default Qualification