import Link from 'next/link'
import cls from 'classnames'
import {useRouter} from 'next/router'
import React, { useEffect, useState } from 'react'



const NavBar = () => {
    let router = useRouter()
    let [currentPath, setCurrentPath] = useState('')
    useEffect(() => {
        setCurrentPath(router.pathname)
    }, [router.pathname])

    const linkStyles = path => {
        return cls([
            'px-4', 'py-3', 
            'font-medium',
            {'text-red-500': currentPath === path}, 
            'hover:text-red-500'
        ])
    }
    
    return (
        <nav className='flex bg-gray-300 shadow-sm'>
            <Link href='/'>
                <a className={linkStyles('/')}>Profile</a>
            </Link>
            <Link href='/work'>
                <a className={linkStyles('/work')}>Work</a>
            </Link>
            <Link href='/blog'>
                <a className={linkStyles('/blog')}>Blog</a>
            </Link>
            <Link href='/contact'>
                <a className={linkStyles('/contact')}>Find Me</a>
            </Link>
        </nav>
    )
}

export default NavBar