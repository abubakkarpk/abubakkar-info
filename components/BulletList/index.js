import React from 'react'
import PropTypes from 'prop-types'

const BulletList = ({ name, items }) => {
    return (
        <div>
            {
                name ? <p className='font-medium'>{name}:</p> : null 
            }
            <ul className='list-disc'>
                {items.map((item, idx) => (
                    <li className='ml-5' key={idx}>{item}</li>
                ))}
            </ul>
        </div>
    )
}

BulletList.propTypes = {
    name: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.string)
}

export default BulletList