import React from 'react'
import PropTypes from 'prop-types'

const Skill = ({ text }) => {
    return (
        <p className='px-3 py-2'>&#9755; {text}</p>
    )
}

Skill.propTypes = {
    text: PropTypes.string.isRequired
}

export default Skill