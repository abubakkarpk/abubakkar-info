import React from 'react'
import PropTypes from 'prop-types'

import Skill from 'components/CoreSkills/Skill'

const SkillGroup = props => {
    let {name, skills} = props.group
    return (
        <div className='flex flex-col w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5'>
            <p className='px-3 py-2 font-bold'>{name}</p>
            {
                skills.map((skill, idx) => <Skill text={skill} key={idx} />)
            }
        </div>
    )
}

SkillGroup.defaultProps = {
    group: {
        name: '',
        skills: []
    }
}

SkillGroup.propTypes = {
    group: PropTypes.shape({
        name: PropTypes.string,
        skills: PropTypes.arrayOf(PropTypes.string)
    }),
}

export default SkillGroup