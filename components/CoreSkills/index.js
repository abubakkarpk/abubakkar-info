import React from 'react';
import PropTypes from 'prop-types'

import SkillGroup from './SkillGroup';
import SectionHeader from 'components/SectionHeader';

const CoreSkills = ({ skillGroups }) => {
    return (
        <section id='core-skills'>
            <SectionHeader text='Core Skills' />
            <div className='flex flex-wrap'>
                {
                    skillGroups.map(group => <SkillGroup group={group} key={group.id} />)
                }
            </div>
        </section>
    )
}

CoreSkills.defaultProps = {
    skillGroups: []
}

CoreSkills.propTypes = {
    skillGroups: PropTypes.array,
}

export default CoreSkills
