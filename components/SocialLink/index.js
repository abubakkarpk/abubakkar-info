import React from 'react'
import PropTypes from 'prop-types'

const SocialLink = ({ link, img, text }) => {
  return (
    <a href={link} className='flex items-center' rel='noreferrer noopener' target='_blank'>
      <img src={img.src} alt={img.alt} className='w-10 mr-1' />
      @{text}
    </a>
  )
}

SocialLink.propTypes = {
  link: PropTypes.string,
  text: PropTypes.string,
  img: PropTypes.shape({
    src: PropTypes.string,
    alt: PropTypes.string,
  }),
}

export default SocialLink