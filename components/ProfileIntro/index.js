import React from 'react'
import PropTypes from 'prop-types'

const ProfileIntro = props => {
    let { image, name, role, email, phone, location } = props.profile
    return (
        <div className='flex flex-col sm:flex-row items-center space-x-4 p-3'>
            <img src={image} alt='Profile Image' className='w-40 rounded-full shadow-md border'/>
            <div className='text-gray-800'>
                <h1 className='font-bold text-3xl uppercase'>{name}</h1>
                <h2 className='font-bold text-lg'>{role}</h2>
                <table className='text-sm'>
                    <tbody>
                    <tr>
                        <th className='text-left w-20'>Email:</th>
                        <td>{email}</td>
                    </tr>
                    <tr>
                        <th className='text-left w-20'>Phone:</th>
                        <td>{phone}</td>
                    </tr>
                    <tr>
                        <th className='text-left'>Location:</th>
                        <td>{location}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

ProfileIntro.propTypes = {
    profile: PropTypes.shape({
        image: PropTypes.string,
        name: PropTypes.string,
        role: PropTypes.string,
        email: PropTypes.string,
        phone: PropTypes.string,
        location: PropTypes.string,
    })
}

export default ProfileIntro