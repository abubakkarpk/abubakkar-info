import React from 'react'
import PropTypes from 'prop-types'

const SectionHeader = ({ text }) => {
    return (
        <h2 className='text-gray-800 font-bold text-2xl border-b-2 pb-2 mb-2'>{text}</h2>
    )
}

SectionHeader.propTypes = {
    text: PropTypes.string,
}

export default SectionHeader