import React from 'react'
import Head from 'next/head'

import SectionHeader from 'components/SectionHeader'
import ContactForm from 'components/ContactForm'
import SocialLink from 'components/SocialLink'


const ContactPage = props => {

  return (
    <div>
      <Head>
          <title>Abu Bakkar Siddique | Find Me</title>
          <link rel='icon' href='/favicon.ico' />
      </Head>
      <div className='flex flex-wrap justify-between'>
        <div className='w-full md:w-5/12'>
          <SectionHeader text='Leave me a message' />
          <ContactForm />
        </div>
        <div className='w-full mt-5 md:mt-0 md:w-5/12'>
          <SectionHeader text='Follow me' />
          <div className="flex flex-col space-y-5">
            <SocialLink 
              link='https://bitbucket.org/abubakkarpk/'
              img={{src: '/icons/bitbucket.png', alt: 'Bitbucket Icon'}}
              text='bitbucket'
            />
            <SocialLink 
              link='https://twitter.com/abubakkarsdq'
              img={{src: '/icons/twitter.png', alt: 'Twitter Icon'}}
              text='twitter'
            />
            <SocialLink 
              link='https://www.instagram.com/abubakkarpk/'
              img={{src: '/icons/instagram.png', alt: 'Instagram Icon'}}
              text='instagram'
            />
            <SocialLink 
              link='https://www.linkedin.com/in/abu-bakkar-siddique-92747a167/'
              img={{src: '/icons/linkedin.png', alt: 'Linkedin Icon'}}
              text='linkedin'
            />
            <SocialLink 
              link='https://www.facebook.com/m.abubakkar.pk'
              img={{src: '/icons/facebook.png', alt: 'Facebook Icon'}}
              text='facebook'
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactPage