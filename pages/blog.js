import React from 'react'
import Head from 'next/head'

const BlogPage = props => {
  return (
    <div>
      <Head>
          <title>Abu Bakkar Siddique | Blog</title>
          <link rel='icon' href='/favicon.ico' />
      </Head>
    </div>
  )
}

export default BlogPage