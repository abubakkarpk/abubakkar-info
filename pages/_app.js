import ProfileIntro from 'components/ProfileIntro'
import NavBar from 'components/NavBar'
import FooterSection from 'components/FooterSection'

import 'styles/tailwind.css'

function MyApp({ Component, pageProps }) {

  return (
    <div className='w-11/12 sm:w-10/12 md:w-9/12 lg:w-8/12 mx-auto'>
      <ProfileIntro  
          profile={{
              image: '/images/profile.jpg',
              name: 'Abu Bakkar Siddique',
              role: 'Full-Stack Web Developer',
              email: 'muhammad.abubakkar@outlook.com',
              phone: '+92 335 8400074',
              location: 'Pakistan',
          }}
      />
      <NavBar />
      <div className='my-5'>
        <Component {...pageProps} />
      </div>
      <FooterSection />
    </div>
  )
}

export default MyApp
