import Head from 'next/head'


import CoreSkills from 'components/CoreSkills'
import CareerSummary from 'components/CareerSummary'
import Qualification from 'components/Qualification'

export default function Home() {
    return (
        <div id='profile'>
            <Head>
                <title>Abu Bakkar Siddique | Profile</title>
                <link rel='icon' href='/favicon.ico' />
            </Head>

            <div className='space-y-5'>
                <section className='my-5 space-y-5'>
                    <p>
                        I am a professional, goal oriented and self-motivated full-stack web developer, with a track record of delivering
                        excellent performance using my proven abilities and skills. With several years of development experience
                        gained working for a number of leading national and international companies, I have developed large scale applications
                        including e-commerce services.
                    </p>  
                </section>

                <CoreSkills 
                    skillGroups={[
                        {id: '1', name: 'Front-End', skills: [
                            'HTML5 / CSS3',
                            'JavaScript / ES6+',
                            'React.js / Next.js',
                        ]},
                        {id: '2', name: 'Back-End', skills: [
                            'Node.js / Express',
                            'PHP / Laravel',
                            'Golang',
                        ]},
                        {id: '3', name: 'Database', skills: [
                            'MySQL',
                            'PostgreSQL',
                            'MongoDB',
                        ]},
                        {id: '4', name: 'DevOps', skills: [
                            'Docker',
                            'Kubernetes',
                            'AWS (Cloud)'
                        ]},
                        {id: '5', name: 'Management', skills: [
                            'Git / Bitbucket',
                            'Jira',
                            'Slack',
                        ]},
                    ]}
                />
                <CareerSummary 
                    jobs={[
                        {
                            id: '23423423423423423423',
                            duration: {from: 'Jun 2020', to: 'Present'},
                            company: {name: 'iPlex', type: 'Web Development Agency'},
                            summary: 'I worked as a Senior JavaScript Developer. My role was to design and develop client-side application using React.js and Next.js',
                            responsibilities: [
                                'Design layouts provided by clients (Figma, XD, Photoshop)',
                                'Integrate APIs from back-end or 3rd party services',
                                'Write unit and integration tests',
                                'Improve user experience',
                                'Improve application performance',
                                'Improve SEO'
                            ],
                            projects: [
                                'Fabric Copilot (E-Commerce)'
                            ],
                            achievements: [],
                        },
                        {
                            id: '23423423423423423',
                            duration: {from: 'May 2018', to: 'Mar 2020'},
                            company: {name: 'Diwaar Inc', type: 'Media Company'},
                            summary: 'I worked as a Full-Stack PHP Developer. My role was to lead a team of developers to design and develop video streaming application using React.js and Laravel framework.',
                            responsibilities: [
                                'Work with designers to develop front-end of application',
                                'Develop APIs to provide services',
                                'Write tests to ensure stablility and quality',
                                'Lead my team to complete the project in time',
                                'Integrate social media platforms for marketing',
                            ],
                            projects: [
                                'Diwaar Network'
                            ],
                            achievements: [
                                'Employee of the year award',
                            ],
                        }
                    ]}
                />
                <Qualification 
                    list={[
                        'BS Software Engineering',
                    ]}
                />
            </div>
        </div>
    )
}
