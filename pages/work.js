import React from 'react'
import Head from 'next/head'

const WorkPage = props => {
  return (
    <div>
      <Head>
          <title>Abu Bakkar Siddique | Work</title>
          <link rel='icon' href='/favicon.ico' />
      </Head>
    </div>
  )
}

export default WorkPage